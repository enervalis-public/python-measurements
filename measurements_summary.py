from dataclasses import dataclass


@dataclass
class MeasurementsSummary:
    total_number_of_measurements: int
    total_number_of_power_entries: int
    absolute_power_min: float
    absolute_power_max: float
    average_power: float
