from typing import List, Optional

from data import Measurement
from measurements_summary import MeasurementsSummary


class MeasurementsModel(object):
    def __init__(self, measurements: List[Measurement]):
        self.__measurements = measurements

    def get_groups_measurements(self, group_id: Optional[str] = None, group_ids: List[str] = []) -> List[Measurement]:
        if group_id != None:
            group_ids.append(group_id)

        return [meas for meas in self.__measurements if meas.deviceGroup in group_ids]

    def get_measurement_summary(self) -> MeasurementsSummary:
        raise NotImplementedError

    def print_measurements(self):
        for meas in self.__measurements:
            print(meas)
