from typing import List

from data import Measurement


def load_json() -> List[Measurement]:
    with open('json/measurements.json', "rt") as measurements:
        return Measurement.schema().loads(measurements.read(), many=True)
