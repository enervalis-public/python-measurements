from measurement_model import MeasurementsModel
from measurements_reader import load_json

if __name__ == '__main__':
    measurements = load_json()
    model = MeasurementsModel(measurements)

    print(" ### REPORT 1 ###")
    for meas in model.get_groups_measurements():
        print(meas)

    print(" ### REPORT 2 ###")
    for meas in model.get_groups_measurements('group_a'):
        print(meas)

    print(" ### REPORT 3 ###")
    for meas in model.get_groups_measurements('group_b'):
        print(meas)

    print(model.get_measurement_summary())
