# Measurements

The file `measurements.json` contains a file with a list of measurements on devices.
Devices can use or produce energy (in/out). 
Devices are grouped in two device groups: _group_a_ and _group_b_.

## Python

- The project is tested and compatible with python 3.8 (3.7 and 3.9 should also work)
- Dependencies listed in requirements.txt

## Goals

- implement calculating the measurements summary, data format defined in `measurements_summary.py`
- make sure float data from `Measurement` can be formatted (number of decimal digits) when printed
- refactor printing report 1,2 and 3 in the main function, allowing for multiple print options
- make sure `get_measurement_summary` works
- refactor loading of the data in `measurements_reader.py` to make it easier to extend and test
- make sure measurements are unique based on : `resourceId | deviceName | deviceGroup | direction`
- improve `print_measurements` in the `MeasurementsModel`
- Write a method that prints the totals for both groups for in and outgoing power

## References

https://pypi.org/project/dataclasses-json/