from dataclasses import dataclass
from enum import Enum
from typing import List

from dataclasses_json import dataclass_json, Undefined


class Direction(Enum):
    IN = "in"
    OUT = "out"


@dataclass_json(undefined=Undefined.RAISE)
@dataclass(frozen=True)
class Power:
    min: float
    max: float
    avg: float
    timestamp: int


@dataclass_json(undefined=Undefined.RAISE)
@dataclass(frozen=True)
class Measurement:
    resourceId: str
    deviceName: str
    deviceGroup: str
    direction: Direction
    power: List[Power]

    @property
    def mean(self):
        return sum([p.avg for p in self.power]) / self.number_power_values

    @property
    def number_power_values(self):
        return len(self.power)

    def __str__(self):
        return f"{self.resourceId} {self.deviceName} {self.deviceGroup} {self.direction}: {self.number_power_values} | {self.mean}"

    def __repr__(self):
        return f"{self.resourceId} {self.deviceName} {self.deviceGroup} {self.direction}: {self.number_power_values} | {self.mean}"
